from django.db import models
from django.conf import settings
from django.contrib.auth import get_user_model

User = get_user_model()


class Devices(models.Model):
    '''
    Links user with devices (which are identified by id with user)
    '''

    user = models.ForeignKey(settings.AUTH_USER_MODEL, blank=False, on_delete=models.CASCADE)
    name = models.CharField(max_length=250, blank=True, default="")
    public_reader_json = models.CharField(max_length=1024, blank=True, default="", unique=True)

    @classmethod
    def get_user_by_device_public_key(cls, public_reader_json: str):
        '''
        Returns user instance by given device public key (this is representaion of Channel Reader instance)
        :param public_key:
        :return:
        '''

        try:

            user = cls.objects.get(public_reader_json=public_reader_json).user

        except Devices.DoesNotExist:
            new_user = User.objects.create_user(username=("AUTO" + str(hash(public_reader_json))))
            new_device = cls.objects.create(user=new_user,
                                            name="Automatically created",
                                            public_reader_json=public_reader_json)
            user = new_device.user

        return user

    def __str__(self):
        return self.name + " at " + self.user.username + " [ " + self.public_reader_json + " ]"


