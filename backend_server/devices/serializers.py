from rest_framework import status, serializers
from .models import Devices


class DeviceSerializer(serializers.Serializer):
    """ Serialize device for access server"""
    name = serializers.CharField(required=False)
    public_reader_json = serializers.CharField(required=True)







