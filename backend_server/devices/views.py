import pickle
from django.contrib.auth import get_user_model
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.generics import ListAPIView
from rest_framework.permissions import AllowAny
from rest_framework.renderers import JSONRenderer

from .models import Devices
from .serializers import DeviceSerializer

User = get_user_model()


class ListMyDevices(ListAPIView):
    '''
    List user devices. Used for Nutouch Access Manager
    '''
    permission_classes = (AllowAny,)
    serializer_class = DeviceSerializer
    model = Devices

    def get(self, request, *args, **kwargs):
        user = request.user
        self.queryset = Devices.objects.filter(user=user)
        print("WEWEW")
        print(user)
        return super().get(request, *args, **kwargs)


class RegisterDeviceView(APIView):
    '''
    Register specific devices for user
    '''

    def post(self, request):
        serializer = DeviceSerializer(data=request.data)
        if serializer.is_valid():
            print(serializer.data)
            name = serializer.data['name']
            public_reader_json_encoded = serializer.data['public_reader_json']
            public_reader_json = pickle.loads(bytes.fromhex(public_reader_json_encoded))
            user = request.user
            print("USer" + str(user))
            device, created = Devices.objects.get_or_create(public_reader_json=public_reader_json)
            device.user = user
            device.name = name
            device.save()

            return Response(status=status.HTTP_200_OK)
        return Response(status=status.HTTP_400_BAD_REQUEST)


class GetNameDeviceView(APIView):
    '''
    Get name of current device. If user press "Register this device" and this devices is already registered
    It changes the name of device
    '''

    renderer_classes = (JSONRenderer, )

    def post(self, request):
        serializer = DeviceSerializer(data=request.data)

        if serializer.is_valid():
            print(serializer.data)
            public_reader_json = serializer.data['public_reader_json']
            user = request.user
            print("USer" + str(user))
            data = {}
            try:
                device = Devices.objects.get(user=user, public_reader_json=public_reader_json)
                data['name'] = device.name
                data['new_device'] = False

            except Devices.DoesNotExist:
                data['name'] = "New Device"
                data['new_device'] = True

            return Response(data=data, status=status.HTTP_200_OK)
        return Response(status=status.HTTP_400_BAD_REQUEST)


