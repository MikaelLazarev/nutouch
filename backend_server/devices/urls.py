from django.urls import path
from .views import ListMyDevices, RegisterDeviceView, GetNameDeviceView

app_name = 'devices'

urlpatterns = [
    path('', ListMyDevices.as_view()),
    path('register/', RegisterDeviceView.as_view()),
    path('get/', GetNameDeviceView.as_view()),
]