from django.contrib.auth import get_user_model
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.generics import ListAPIView
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.renderers import JSONRenderer

from .models import Sites
from devices.models import Devices
from .serializers import UrlSerializer

User = get_user_model()


class GetLabelView(APIView):
    '''
    Serializer which returns channel by url & reader_pubkey.
    It uses devices to figure out a user instances using Devices model
    '''
    permission_classes = (AllowAny,)
    serializer_class = UrlSerializer
    #renderer_classes = (JSONRenderer,)

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data,
                                           context={'request': request})
        serializer.is_valid(raise_exception=True)

        url = serializer.validated_data['url']
        public_reader_json = serializer.validated_data['public_reader_json']

        print("Get request for site identity: \n" + url)
        print("From: \n" + public_reader_json)

        user = Devices.get_user_by_device_public_key(public_reader_json)
        print("USER!!!!!")
        print(str(user.username))


        site = Sites.get_or_create_label_by_url(user=user, url=url)
        site.grant_access(public_reader_json)

        return Response(site.channel, status=status.HTTP_200_OK)


class ListMySites(ListAPIView):
    '''
    Depreciated serializer
    '''
    permission_classes = (AllowAny,)
    serializer_class = UrlSerializer
    model = Sites

    def get(self, request, *args, **kwargs):
        user = request.user
        self.queryset = Sites.objects.all()
        print(user)
        return super().get(request, *args, **kwargs)
