from rest_framework import status, serializers
from .models import Sites


class UrlSerializer(serializers.Serializer):
    """ Serialize urls from requests"""
    url = serializers.CharField(required=False)
    public_reader_json = serializers.CharField(required=False)







