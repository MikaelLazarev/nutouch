import sha3
import maya
import random
from django.db import models
from django.conf import settings
from crypto.manager import ChannelManager
from crypto.reader import ChannelReaderPublic
from crypto.channel import Channel
from devices.models import Devices


class Sites(models.Model):
    '''
    Stores Channel serialized information for specific users + sites
    '''

    url = models.CharField(max_length=100, default="", blank=True)
    channel = models.CharField(max_length=5000, default="", blank=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, blank=False, on_delete=models.CASCADE)

    @classmethod
    def get_or_create_label_by_url(cls, user: object, url: str) -> str:
        '''
        Returns channel info by getting user & url. If channels is not exists, it creates a new one and grant access to
        all user devices
        :param user:
        :param url:
        :return: Channel serialized into JSON
        '''

        site, created = cls.objects.get_or_create(user=user, url=url)
        if created:
            # getting ChannelManager instance (it's a Singleton class)
            manager = ChannelManager()

            # Creating new channel and save it's str representaion into db
            channel = manager.create_new_channel()

            # Collecting all user devices
            devices = Devices.objects.filter(user=user).all()

            # Granted access to all devices
            for device in devices:
                device_reader = ChannelReaderPublic.from_json(device.public_reader_json)
                manager.grant(device_reader, channel)

            site.channel = channel.to_json()
            site.save()

        return site

    def grant_access(self, public_reader_json):
        '''
        Grants access for the channel
        :param public_reader_json:
        :return:
        '''
        manager = ChannelManager()
        channel = Channel.from_json(self.channel)
        device_reader = ChannelReaderPublic.from_json(public_reader_json)
        manager.grant(device_reader, channel)

    def __str__(self):
        return self.url + " [channel: " + self.channel + " , " + " user: " + self.user.username + " ]"
