from django.urls import path
from .views import GetLabelView, ListMySites


app_name = 'sites'

urlpatterns = [
    path('get_label/', GetLabelView.as_view()),
    path('list/', ListMySites.as_view()),
]