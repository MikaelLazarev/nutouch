from rest_framework import permissions
from django.contrib.auth.mixins import UserPassesTestMixin
from django.contrib.auth.decorators import user_passes_test

class EmailIsVerified(permissions.BasePermission):
    message = 'Email is not verified'

    def has_permission(self, request, view):
        user = request.user
        return user.is_verified


class IsStaffMixin(UserPassesTestMixin):

    def test_func(self):
        return self.request.user.is_staff

def staff_required():
    return user_passes_test(lambda u: u.is_staff, login_url="/accounts/login")