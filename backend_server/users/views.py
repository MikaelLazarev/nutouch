
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework_simplejwt.views import TokenObtainPairView

from .serializers import SignUpSerializer

class SignUpAPI(APIView):
    """
    Sign up process for new users. It creates a new User instance
    And send verification email
    """
    permission_classes = (AllowAny,)
    serializer_class = SignUpSerializer

    def post(self, request):
        user = request.data
        print(user)

        # The create serializer, validate serializer, save serializer pattern
        # below is common and you will see it a lot throughout this course and
        # your own work later on. Get familiar with it.
        serializer = self.serializer_class(data=user)
        serializer.is_valid(raise_exception=True)
        try:
            new_user = serializer.save()
        except ValueError as e:
            print(str(e))
            return Response(str(e), status.HTTP_400_BAD_REQUEST)

        return Response(serializer.data, status=status.HTTP_201_CREATED)



'''
class Login(TokenObtainPairView):
    """This class is modified to add Login event"""

    def post(self, request, *args, **kwargs):
        response = super(Login, self).post(request, *args, **kwargs)
        print("Login response", response)
        if response.status_code == 200:
            user = User.objects.get(email=request.data['email'])
        return response
'''