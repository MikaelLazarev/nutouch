from django.urls import include, path
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView
from .views import SignUpAPI

urlpatterns = [

    path('', include('rest_framework.urls', namespace='rest_framework')),
    path('signup/', SignUpAPI.as_view()),

]



