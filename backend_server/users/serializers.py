from django.conf import settings
from django.contrib.auth import get_user_model
from rest_framework import serializers

class SignUpSerializer(serializers.ModelSerializer):

    email = serializers.EmailField(
        write_only=True
    )
    # Ensure passwords are at least 8 characters long, no longer than 128
    # characters, and can not be read by the client.
    password = serializers.CharField(
        max_length=128,
        min_length=8,
        write_only=True
    )

    class Meta:
        model = get_user_model()
        fields = ['email', 'password']

    def create(self, validated_data):
        # Use the `create_user` method we wrote earlier to create a new user.
        user = validated_data['email']
        password = validated_data['password']
        return get_user_model().objects.create_user(username=user, email=user, password=password)

