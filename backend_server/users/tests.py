from django.test import TestCase, Client
from django.core import mail
from rest_framework import status
from .models import User



class UserTest(TestCase):
    """
    Testing user creation process

    """
    def setUp(self):
        # creating superuser
        self.user = User.objects.create_superuser("superuser@me.com", "superuser@me.com", "Qwert12345678")
        # Checking that tokens were created
        self.user = User.objects.create_user("test@me.com", "test@me.com", "Qwert12345678")


    def test_users_were_created(self):
        self.assertEqual(User.objects.count(), 2)

