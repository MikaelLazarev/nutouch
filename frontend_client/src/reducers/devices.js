import { updateState } from "../utils/updateState";

import * as actions from '../actions/actions'
import * as status from '../config'

const initialState = {
    id: {}
  }



export default (state=initialState, action) => {


  console.log(action)
  switch(action.type) {

    // Data operations
    case actions.GET_ID_REQUEST:

        return {
            id: {
                status: status.STATUS_LOADING,
                id: null
            }
        }

    case actions.GET_ID_SUCCESS:

       return {
            id: {
                status: status.STATUS_SUCCESS,
                id: action.payload.username
            }
        }

    default:
        return state
  }
}

export const getId  = (state) => state.id