import React from 'react'
import { connect } from 'react-redux'

import * as status from '../../config'
import * as actions from '../../actions/showid'
import * as reducers from '../../reducers'

import Preloader from '../../components/UI/Preloader'

class ShowId extends React.Component {


  componentDidMount() {
    this.props.onInit()
  }

  render() {

      console.log(this.props.id)

      if (this.props.id !== undefined &&
          this.props.id.status === status.STATUS_SUCCESS)
          return <h1 align="center">Hello, {this.props.id.id }, world!</h1>
      return <h1>Hello</h1>
  }


}

const mapStateToProps = (state) => ({

  id: reducers.getId(state),

})

const mapDispatchToProps = dispatch => {
  return {
    onInit: () => dispatch(actions.getId())
  }
};


export default connect(mapStateToProps, mapDispatchToProps)(ShowId);
