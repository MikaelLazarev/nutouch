export const LOGIN_REQUEST = '@@jwt/LOGIN_REQUEST';
export const LOGIN_SUCCESS = '@@jwt/LOGIN_SUCCESS';
export const LOGIN_FAILURE = '@@jwt/LOGIN_FAILURE';

export const SIGNUP_REQUEST = '@@jwt/SIGNUP_REQUEST';
export const SIGNUP_SUCCESS = '@@jwt/SIGNUP_SUCCESS';
export const SIGNUP_FAILURE = '@@jwt/SIGNUP_FAILURE';

export const TOKEN_REQUEST  = '@@jwt/TOKEN_REQUEST';
export const TOKEN_RECEIVED = '@@jwt/TOKEN_RECEIVED';
export const TOKEN_FAILURE  = '@@jwt/TOKEN_FAILURE';

export const LOGOUT        = '@@jwt/LOGOUT';

export const DEVICESLIST_REQUEST = '@@DEVICES/LIST_REQUEST';
export const DEVICESLIST_SUCCESS = '@@DEVICES/LIST_SUCCESS';
export const DEVICESLIST_FAILURE = '@@DEVICES/LIST_FAILURE';

export const DEVICENAME_REQUEST = '@@DEVICES/NAME_REQUEST';
export const DEVICENAME_SUCCESS = '@@DEVICES/NAME_SUCCESS';
export const DEVICENAME_FAILURE = '@@DEVICES/NAME_FAILURE';

export const GET_ID_REQUEST = '@@DEVICES/REGISTER_REQUEST';
export const GET_ID_SUCCESS = '@@DEVICES/REGISTER_SUCCESS';
export const GET_ID_FAILURE = '@@DEVICES/REGISTER_FAILURE';
