import { RSAA } from 'redux-api-middleware';
import { withAuth } from '../reducers'
import { getFullAPIAddress } from '../config'
import { error, success } from 'react-notification-system-redux';
import * as actions from "./actions"


export const getId = () => ({
    [RSAA]: {
        endpoint: getFullAPIAddress( '/api/auth/id/' ),
        method: 'POST',
        headers: withAuth({ 'Content-Type': 'application/json' }),
        types: [
            actions.GET_ID_REQUEST, actions.GET_ID_SUCCESS, actions.GET_ID_FAILURE
        ]
      }
})


