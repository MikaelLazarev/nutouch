import React from 'react';
import Router, { Link, goBack } from 'route-lite';
import DevicesList from '../../containers/ShowId/ShowId'

const Page404 = () => {
    return (<React.Fragment>
                <h1> Page not Found </h1><br />
                <p>You can open <Link component={DevicesList}>Devices List</Link></p>
            </React.Fragment>
            );



}

export default Page404;