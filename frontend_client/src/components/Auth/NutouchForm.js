import React, { Component } from 'react'
import { Alert, Button, Form, Panel } from 'react-bootstrap';
import TextField from './TextField'
import './LoginForm.css'


export default class NutouchForm extends Component {
  state = {
    devicename: '',
    public_reader_json: '',
    disabled_submit: true
  }

  handleInputChange = (event) => {
    const target = event.target,
      value = target.type === 'checkbox' ? target.checked : target.value,
      name = target.name

    const disabled_submit = ((name==="devicename") && (value.length==0)) ? true: false;


    this.setState({
      [name]: value,
      'disabled_submit': disabled_submit
    });


  }

  onSubmit = (event) => {
    event.preventDefault()
    this.props.onSubmit(this.state.devicename, window.nutouch.public_reader_json)
  }

  componentDidMount() {

    this.setState({devicename: this.props.defaultName})

  }

  render() {
    const errors = this.props.errors || {}
    let title;

    if (this.props.new_device) title = "Register New Device"
      else title = "Edit device name"

    return (
      <div className={'container-group'}>

      <Panel className={'login-panel'}>
            <h2 className={'welcome-text'}>{ title }</h2>

            <Form onSubmit={this.onSubmit}>
            
              {
              errors.non_field_errors ?
                <Alert color="danger">
                    {errors.non_field_errors}
                </Alert>:""
              }
              <TextField name="devicename"
                        placeholder={this.props.defaultName}
                        error={errors.name}
                        onChange={this.handleInputChange} 
                        className="login-input"
                        />

              <TextField name="public_reader_json"
                         value={window.nutouch.public_reader_json}
                         style={{height: 150}}
                         componentClass="textarea"
                         className="login-input"
                         disabled={true}/>

              <Button type="submit"
                      size="lg"
                      bsStyle="primary"
                      style={{minWidth: '100px'}}
                      disabled={this.state.disabled_submit}  >
                 REGISTER
              </Button>&nbsp;&nbsp;

            </Form>
    </Panel>
          

</div>

    )
  }
}
