import React, { Component } from 'react'
import { Link } from "react-router-dom"

import {Alert, Button, Form, MenuItem, NavItem, Panel} from 'react-bootstrap';
import TextField from './TextField'
import './LoginForm.css'

export default class LoginForm extends Component {
  state = {
    username: '',
    password: ''
  }

  handleInputChange = (event) => {
    const target = event.target,
      value = target.type ===
      'checkbox' ? target.checked : target.value,
      name = target.name
    this.setState({
      [name]: value
    });
  }

  onSubmit = (event) => {
    event.preventDefault()
    this.props.onSubmit(this.state.username, this.state.password)
  }

  onTouch = (event) => {
    event.preventDefault()
    this.props.onTouch()
  }

  componentWillUpdate(nextProps, nextState, nextContext) {

    // Disable autorenewal after getting Nutouch instance
    if (window.nutouch != undefined &&
        window.nutouch.public_reader_json !== undefined &&
        window.nutouch.public_reader_json !== null) {
           clearInterval(this.interval);
        }
  }

  componentDidMount() {
      this.interval = setInterval(() => this.setState({ time: Date.now() }), 1000);
    }


  render() {
    const errors = this.props.errors || {}
    console.log( window)
    let nutouchButton = ""

    if (window.nutouch != undefined) {
            nutouchButton = <Button type="submit"
                      size="lg"
                      bsStyle="success"
                      style={{minWidth: '100px'}}
                      onClick={this.onTouch}  >
                 TOUCH WITH NUTOUCH
              </Button>
    }
    return (




      <div className={'container-group'}>

   

      <Panel className={'login-panel'}>
            <h2 className={'welcome-text'}>Log into NuTouch DEMO CLIENT</h2>

            <Form onSubmit={this.onSubmit}>
            
              {
              errors.non_field_errors ?
                <Alert color="danger">
                    {errors.non_field_errors}
                </Alert>:""
              }
              <TextField name="username" 
                        placeholder="E-mail" 
                        error={errors.username}
                        onChange={this.handleInputChange} 
                        className="login-input"
                        />

              <TextField name="password"
                        placeholder="Password" 
                        error={errors.password} type="password"  
                        onChange={this.handleInputChange} 
                        className="login-input" />

              <Button type="submit"
                      size="lg"
                      bsStyle="primary"
                      style={{minWidth: '100px'}}  >
                 SIGN IN
              </Button>&nbsp;&nbsp;

                <h5>New to Nuchat? &nbsp;
                 <Link to={'/signup/'}>
                   Sign Up
                 </Link>
                </h5>
                {nutouchButton}
                &nbsp;&nbsp;
                
            </Form>
    </Panel>
          

</div>

    )
  }
}
