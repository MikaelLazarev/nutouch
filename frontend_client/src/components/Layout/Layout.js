import React from 'react';
import { Route, Switch, Redirect } from 'react-router';
import Menu from '../Menu/Menu'
import Footer from './Footer'
import Login from "../../containers/Auth/Login";
import SignUp from "../../containers/Auth/SignUp";
import Page404 from "../HelpPage/404";
import DevicesList from "../../containers/ShowId/ShowId";

const layout = (props) => {

    let routes;

    if (!props.isAuthenticated) {
        routes = <Switch>
            <Route path="/login/" component={Login}/>
            <Route path="/signup/" component={SignUp}/>
            <Route component={Login}/>
        </Switch>
    }
    else {
        routes = <Switch {...props}>

                    {/* Investors menu */}
                    <Route exact path="/id/" component={() => <DevicesList {...props} />}/>

                    {/* Default action & Page 404 */}
                    <Route exact path="/" render={() => (<Redirect to="/id"/>)}/>
                    <Route exact path="/login" render={() => (<Redirect to="/id"/>)}/>
                    <Route path='*' component={Page404}/>
        </Switch>
        }

    return (<React.Fragment>
                <Menu {...props} />
                    {routes}
                <Footer {...props}/>
            </React.Fragment>);
}

export default layout;
