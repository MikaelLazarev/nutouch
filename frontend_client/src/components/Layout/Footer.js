import React from "react";
import './Footer.css'

const footer = ( props ) => (
        <footer className="footer navbar-fixed-bottom">
            <div className="container">
                <div className="row">
                    <div className="col-xs-6">
                        2019 © Mikhail Lazarev, All rights reserved
                    </div>
                    <div className="col-xs-6">
                        <ul className="pull-right list-inline" >

                            <li>
                                <a href="http://t.me/mikael_l" target="_blank" rel="noopener noreferrer">Contact</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>);


export default footer;
