import React from "react";
import {MenuItem, Nav, Navbar, NavItem} from 'react-bootstrap';
import { Link } from "react-router-dom";
import "./Menu.css"


export class Menu extends React.Component {

    render () {
        let rightMenu, register_this_device;

        if (this.props.isAuthenticated) {
            rightMenu = <Nav pullRight className='navbar_header'>
                {register_this_device}
                <MenuItem eventKey={3.1} onClick={this.props.onLogout}>
                    <NavItem className='nav-item' onClick={this.props.onLogout}>LOGOUT</NavItem>
                </MenuItem>
            </Nav>
        } else
            rightMenu = <Nav pullRight className='navbar_header'>
                <MenuItem eventKey={3.1}>
                    <Link to={'/login'}>SIGN IN</Link>
                </MenuItem>
                <MenuItem eventKey={3.2}>
                    <Link to={'/signup'}>SIGN UP</Link>
                </MenuItem>
            </Nav>;

        return (
            <Navbar>

                {rightMenu}

            </Navbar>
        )
    }

}



export default Menu;
