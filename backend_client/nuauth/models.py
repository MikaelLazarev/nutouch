import base58

from django.db import models
from django.conf import settings
from django.contrib.auth import get_user_model


class NutouchUser(models.Model):
    '''
    Links channels (which is given) with user on Client service.
    It also creates new users if serialized channel wasn't found
    '''
    channel_reader = models.CharField(max_length=5120, default=None, blank=True, null=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True, default=None, on_delete=models.CASCADE)

    @classmethod
    def get_user(cls, channel_reader: str) -> object:
        '''
        Gets user by data_public_key & label. Creates a new user if user with credentials was not found
        :param data_public_key:
        :param label:
        :return: user
        '''
        nutouch_user, created = cls.objects.get_or_create(channel_reader=channel_reader)

        if created:
            nutouch_user.user = get_user_model().objects.create_user(username=channel_reader)
            nutouch_user.save()

        return nutouch_user




