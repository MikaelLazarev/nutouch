from django.urls import path
from .views import TokenObtainNutouchView
from rest_framework_simplejwt.views import TokenRefreshView

app_name = 'nuauth'

urlpatterns = [
    path('token/obtain/', TokenObtainNutouchView.as_view()),
    path('token/refresh/', TokenRefreshView.as_view()),
]