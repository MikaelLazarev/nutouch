from django.utils.six import text_type
from rest_framework.authentication import HTTP_HEADER_ENCODING, TokenAuthentication


class NuAuthenticationBackend(TokenAuthentication):
    '''
    Django NuCypher Authentication backendю
    DEPRICIATED!
    '''

    def authenticate(self, request):
        header = self.get_header(request)
        print(header)
        pass

    def authenticate_header(self, request):
        header = self.get_header(request)
        print(header)

        super().authenticate_header(request)

    def get_header(self, request):
        """
        Extracts the header containing the JSON web token from the given
        request.
        """
        header = request.META.get('HTTP_AUTHORIZATION')

        if isinstance(header, text_type):
            # Work around django test client oddness
            header = header.encode(HTTP_HEADER_ENCODING)

        return header

    def get_token(self):
        pass