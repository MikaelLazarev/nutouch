import pickle
import json
from rest_framework import serializers
from django.contrib.auth import get_user_model
from django.contrib.auth import authenticate
from django.utils.six import text_type
from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers

from rest_framework_simplejwt.tokens import RefreshToken, SlidingToken, UntypedToken
from .models import NutouchUser
from crypto.channel import Channel
from crypto.data_pkg import EncryptedDataPackage

User = get_user_model()


class TokenObtainSerializer(serializers.Serializer):
    username_field = User.USERNAME_FIELD

    def __init__(self, *args, **kwargs):
        super(TokenObtainSerializer, self).__init__(*args, **kwargs)

        self.fields['channel_json'] = serializers.CharField()
        self.fields['channel_reader'] = serializers.CharField()

    def validate(self, attrs):
        '''
        Authenficate user by provided channel. Each one could send info
        with serialized channel, but only granted Channel Readers could decrypt response
        :param attrs:
        :return:
        '''

        print("Obtain request: ")
        print(attrs)
        self.reader = attrs['channel_json']
        self.channel_json = attrs['channel_json']

        self.nutouch_user = NutouchUser.get_user(channel_reader=self.channel_json)
        self.user = self.nutouch_user.user

        # Prior to Django 1.10, inactive users could be authenticated with the
        # default `ModelBackend`.  As of Django 1.10, the `ModelBackend`
        # prevents inactive users from authenticating.  App designers can still
        # allow inactive users to authenticate by opting for the new
        # `AllowAllUsersModelBackend`.  However, we explicitly prevent inactive
        # users from authenticating to enforce a reasonable policy and provide
        # sensible backwards compatibility with older Django versions.
        if self.user is None or not self.user.is_active:
            raise serializers.ValidationError(
                _('No active account found with the given credentials'),
            )
        return {}

    @classmethod
    def get_token(cls, user):
        raise NotImplemented('Must implement `get_token` method for `TokenObtainSerializer` subclasses')


class TokenObtainNutouchPairSerializer(TokenObtainSerializer):

    @classmethod
    def get_token(cls, user):
        return RefreshToken.for_user(user)

    def validate(self, attrs):
        '''
        Return encrypted JWT tokens
        :param attrs:
        :return:
        '''
        data = super(TokenObtainNutouchPairSerializer, self).validate(attrs)
        refresh = self.get_token(self.user)

        # preparing data response like standart JWT response
        data['refresh'] = text_type(refresh)
        data['access'] =  text_type(refresh.access_token)

        # encrypt it for all devices
        channel = Channel.from_json(self.channel_json)
        encrypted_data = EncryptedDataPackage.from_channel(channel, pickle.dumps(data))
        json_data = encrypted_data.to_json()

        return json.loads(json_data)