import { updateState } from "../utils/updateState";

import * as actions from '../actions/actions'
import * as status from '../config'

const initialState = {
    DevicesList: [],
    Device: {},
  }


const updateDeviceList = (state, status, data) => (
    updateState(state, { DevicesList: {
                                                status: status,
                                                data: data
                                                }
                                            })
)


export default (state=initialState, action) => {


  console.log(action)
  switch(action.type) {

    // Data operations
    case actions.DEVICESLIST_REQUEST:

        if ((action.error !== undefined) && (action.error) )
            return updateDeviceList(state, status.STATUS_FAILURE, undefined)

        return updateDeviceList(state, status.STATUS_LOADING, undefined)

      case actions.DEVICESLIST_SUCCESS:

        let updatedList = {};

        for (let item in action.payload) {
                updatedList[item] = {
                    ...action.payload[item],
                    data_status: status.STATUS_SUCCESS,
                }

            }
        return updateDeviceList(state, status.STATUS_SUCCESS, updatedList)

    case actions.DEVICESLIST_FAILURE:
        return updateDeviceList(state, status.STATUS_FAILURE, undefined)

    case actions.DEVICENAME_REQUEST:
          return {
              ...state,
              Device: {
                  status: status.STATUS_LOADING,
                  name: "",
                  new_device: true

              }
          }

    case actions.DEVICENAME_SUCCESS:
          return {
              ...state,
              Device: {
                  status: status.STATUS_SUCCESS,
                  name: action.payload.name,
                  new_device: action.payload.new_device
              }
          }

    default:
        return state
  }
}

export const getDevicesList  = (state) => state.DevicesList
export const getDeviceName   = (state) => state.Device