import { combineReducers } from 'redux'
import auth, * as fromAuth from './auth'
import devices, * as fromDevices from './devices'

export default combineReducers({
    auth: auth,
    devices: devices,
})

// Authenfication
export const isAuthenticated = state => fromAuth.isAuthenticated(state.auth)
export const accessToken = state => fromAuth.accessToken(state.auth)
export const isAccessTokenExpired = state => fromAuth.isAccessTokenExpired(state.auth)
export const refreshToken = state => fromAuth.refreshToken(state.auth)
export const isRefreshTokenExpired = state => fromAuth.isRefreshTokenExpired(state.auth)
export const authErrors = state => fromAuth.errors(state.auth)
export const refreshTime = state => fromAuth.refreshTime(state.auth)
export const getSignupSuccess = state => fromAuth.signupSuccess(state.auth)

// DevicesRegister
export const getDevicesList = state => fromDevices.getDevicesList(state.devices)
export const getDeviceName = state => fromDevices.getDeviceName(state.devices)

export function withAuth(headers={}) {
  return (state) => ({
    ...headers,
    'Authorization': `Bearer ${accessToken(state)}`
  })
}
