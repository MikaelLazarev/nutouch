import React from 'react'
import {Redirect} from "react-router-dom";
import NutouchForm from '../../components/Auth/NutouchForm'
import {connect} from "react-redux";
import * as reducers from "../../reducers";
import * as actions from "../../actions/devices";
import * as status from '../../config'

class DevicesRegister extends React.Component {

  state = {
    redirect: false
  }

  onSubmit = (devicename, public_reader_json) => {
    console.log(devicename)
    console.log(public_reader_json)
    this.props.registerDevice(devicename, public_reader_json)
    this.setState({redirect: true})

  }

  componentWillUpdate(nextProps, nextState, nextContext) {

    console.log("gg", this.props.deviceName.status)
    // Disable autorenewal after getting Nutouch instance
    if (window.nutouch != undefined &&
        window.nutouch.public_reader_json !== undefined &&
        window.nutouch.public_reader_json !== null &&
        this.props.deviceName.status === undefined) {
           clearInterval(this.interval);
           console.log("Send request")
           this.props.getDeviceName(window.nutouch.public_reader_json)
        }
  }

  componentDidMount() {
      this.interval = setInterval(() => this.setState({ time: Date.now() }), 1000);
    }

    componentWillUnmount() {
      clearInterval(this.interval);
    }

  render() {

    if (this.state.redirect) return <Redirect
            to={"/login"}

          />

    if (this.props.deviceName.status === status.STATUS_SUCCESS) {
        return <NutouchForm
                    onSubmit={this.onSubmit}
                    newDivce = {this.props.deviceName.new_device}
                    defaultName = {this.props.deviceName.name}
              />
    }

    console.log(this.props.deviceName)
    return "Loading..."
  }


}

const mapStateToProps = (state) => ({

  devicesList: reducers.getDevicesList(state),
  deviceName: reducers.getDeviceName(state)

})

const mapDispatchToProps = dispatch => {
  return {
    getDeviceName: (public_reader_json) => dispatch(actions.getDeviceName(public_reader_json)),
    registerDevice: (name, public_reader_json) => dispatch(actions.registerDevice(name, public_reader_json))
  }
};


export default connect(mapStateToProps, mapDispatchToProps)(DevicesRegister);
