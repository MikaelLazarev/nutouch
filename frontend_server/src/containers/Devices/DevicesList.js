import React from 'react'
import { Grid, Col, Panel } from 'react-bootstrap'
import Finder from '../../components/Finder/Finder'
import DeviceListLoader from './DevicesListLoader'

class DevicesList extends React.Component {

  state = { chatlabel: null}

  onChatSelection = (chatlabel) => {
      console.log(chatlabel)
      this.setState({...this.state, chatlabel: chatlabel})

  }

  render() {
    var messages;

    if (this.state.chatlabel) {
        messages = "Device details"
    }
    else { messages = <Panel>
                        <Panel.Body>
                            <h2>Choose chat from left bar</h2>
                        </Panel.Body>
                      </Panel>
       }

    return <Grid>
                <Col sm={12}>
                    <DeviceListLoader {...this.props}>
                            <Finder
                                header = 'Connected Devices'
                                choose_chat_func = { this.onChatSelection }
                            />
                    </DeviceListLoader>
                </Col>

            </Grid>
  }


}

export default DevicesList;
