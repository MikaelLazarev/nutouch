import React from 'react'
import { connect } from 'react-redux'

import * as status from '../../config'
import * as actions from '../../actions/devices'
import * as reducers from '../../reducers'

import Preloader from '../../components/UI/Preloader'

class DevicesListLoader extends React.Component {


  componentDidMount() {
    this.props.onInitChatList()
  }

  render() {
    let childrenWithProps = this.props.children,
      itemsList;

    itemsList = this.props.devicesList;

    if (itemsList !== undefined) {

      switch (itemsList.status) {
        case status.STATUS_LOADING:
          childrenWithProps = (
              <div>CHATS LLL
            <Preloader />
              </div>
          )
          break;

        case status.STATUS_SUCCESS:
          const { children } = this.props;

          childrenWithProps = React.Children.map(children, child =>
            React.cloneElement(child, { ...this.props,
              itemsList: itemsList
            }));
          break;

        default:
          childrenWithProps = (
            <Preloader />
          )
          break;

      }
    }

    return <React.Fragment>{ childrenWithProps}</React.Fragment>
  }


}

const mapStateToProps = (state) => ({

  devicesList: reducers.getDevicesList(state),

})

const mapDispatchToProps = dispatch => {
  return {
    onInitChatList: () => dispatch(actions.getDevicesList())
  }
};


export default connect(mapStateToProps, mapDispatchToProps)(DevicesListLoader);
