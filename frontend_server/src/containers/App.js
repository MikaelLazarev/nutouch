import React from "react";
import { withRouter } from 'react-router';
import { connect } from 'react-redux'
import Layout from '../components/Layout/Layout';
import { authErrors, isAuthenticated, refreshTime} from '../reducers';
import { logout } from '../actions/auth';


class App extends React.Component {

  render (){
        return <Layout {...this.props} menuItem = {this.props.menuItem}/>
    }
}

const mapStateToProps = (state) => ({
    errors:          authErrors(state),
    isAuthenticated: isAuthenticated(state),
    refreshTime:     refreshTime(state)

})

const mapDispatchToProps = dispatch => {
  return {
        onLogout:               () => dispatch(logout())
  }
};


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));

