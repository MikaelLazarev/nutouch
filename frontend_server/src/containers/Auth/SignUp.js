import React from 'react'
import { connect } from 'react-redux'
import { goTo } from 'route-lite'

import { signup } from '../../actions/auth'
import { authErrors, isAuthenticated, getSignupSuccess } from '../../reducers/index'
import SignUpForm from '../../components/Auth/SignUpForm'
import Login from './Login'


const SignUp = (props) => {

  if (props.isAuthenticated) goTo(Login, props);

  if (props.signup_success === undefined || !props.signup_success) {
      return <div className="login-page">
          <SignUpForm {...props}/>
      </div>
  }

  return "You were sucessfully signup"

}

const mapStateToProps = (state) => ({
  errors: authErrors(state),
  isAuthenticated: isAuthenticated(state),
  signup_success: getSignupSuccess(state)
})

const mapDispatchToProps = (dispatch) => ({
  onSubmit: (username, password) => dispatch(signup(username, password))

})

export default connect(mapStateToProps, mapDispatchToProps)(SignUp);
