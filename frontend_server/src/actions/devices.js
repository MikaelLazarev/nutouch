import { RSAA } from 'redux-api-middleware';
import { withAuth } from '../reducers'
import { getFullAPIAddress } from '../config'
import { error, success } from 'react-notification-system-redux';
import * as actions from "./actions"



export const getDevicesList = () => {
  console.log('[API Call]: GetDevicesList');
  return ({
  [RSAA]: {
      endpoint: getFullAPIAddress( '/api/devices/' ),
      method: 'GET',
      headers: withAuth({ 'Content-Type': 'application/json' }),
      types: [
        actions.DEVICESLIST_REQUEST, actions.DEVICESLIST_SUCCESS, actions.DEVICESLIST_FAILURE
      ]
  }
});
}

export const getDeviceName = (public_reader_json) => {
  console.log('[API Call]: GetDevicesList');
  return ({
  [RSAA]: {
      endpoint: getFullAPIAddress( '/api/devices/get/' ),
      method: 'POST',
      body: JSON.stringify({public_reader_json}),
      headers: withAuth({ 'Content-Type': 'application/json' }),
      types: [
        actions.DEVICENAME_REQUEST, actions.DEVICENAME_SUCCESS, actions.DEVICENAME_FAILURE
      ]
  }
});
}


export const registerDevice = (name, public_reader_json) => ({
    [RSAA]: {
        endpoint: getFullAPIAddress( '/api/devices/register/' ),
        method: 'POST',
        body: JSON.stringify({name, public_reader_json}),
        headers: withAuth({ 'Content-Type': 'application/json' }),
        types: [
            actions.DEVICEREGISTER_REQUEST, actions.DEVICEREGISTER_SUCCESS, actions.DEVICEREGISTER_FAILURE
        ]
      }
})


