import {RSAA} from 'redux-api-middleware';
import {getFullAPIAddress} from '../config'
import {withAuth} from '../reducers'
import * as actions from './actions'
import {notifyError} from "../utils/updateState";
import {LOGIN_SUCCESS} from "./actions";

export const login = (username, password) => (
    {
        [RSAA]: {
            endpoint: getFullAPIAddress('/api/auth/token/obtain/'),
            method: 'POST',
            body: JSON.stringify({username, password}),
            headers: {'Content-Type': 'application/json'},
            types: [actions.LOGIN_REQUEST, actions.LOGIN_SUCCESS, actions.LOGIN_FAILURE]
        }
    }
)

export const signup = (email, password) => ({
    [RSAA]: {
        endpoint: getFullAPIAddress( '/api/auth/signup/' ),
        method: 'POST',
        body: JSON.stringify({email, password}),
        headers: { 'Content-Type': 'application/json' },
        types: [
            actions.SIGNUP_REQUEST, actions.SIGNUP_SUCCESS, actions.SIGNUP_FAILURE
        ]
      }
})


export const refreshAccessToken = (token) => ({
    [RSAA]: {
        endpoint: getFullAPIAddress( '/api/auth/token/refresh/' ),
        method: 'POST',
        body: JSON.stringify({refresh: token}),
        headers: { 'Content-Type': 'application/json' },
        types: [
          actions.TOKEN_REQUEST, actions.TOKEN_RECEIVED, actions.TOKEN_FAILURE
        ]
    }
})

export const nutouch = () =>
    ({
    type: LOGIN_SUCCESS,
    payload: {
        access: window.nutouch.access_token,
        refresh: window.nutouch.refresh_token
    }
})

export const logout = () => ({
    type: actions.LOGOUT
   
})




