import React from "react"
import "./loading.css"

const Preloader = () => (
  <div class="bouncing-loader">
    <div></div>
    <div></div>
    <div></div>
  </div>
);

export default Preloader
