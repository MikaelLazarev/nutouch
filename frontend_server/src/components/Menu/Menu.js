import React from "react";
import {MenuItem, Nav, Navbar, NavItem} from 'react-bootstrap';
import { Link } from "react-router-dom";
import "./Menu.css"


export class Menu extends React.Component {

    componentDidMount() {
      // It checks every second if window.nutouch appears to provive Register this service button
      // Todo: disable this checking when window.nutouch appears
      this.interval = setInterval(() => this.setState({ time: Date.now() }), 1000);
    }
    componentWillUnmount() {
      clearInterval(this.interval);
    }

    render () {
        let rightMenu, register_this_device;

        if (window.nutouch != undefined) {
            register_this_device = <MenuItem eventKey={3.1}>
                <NavItem className='nav-item'>
                    <Link to={'/register'}>REGISTER THIS DEVICE</Link>
                </NavItem>
            </MenuItem>
        }

        if (this.props.isAuthenticated) {
            rightMenu = <Nav pullRight className='navbar_header'>
                {register_this_device}
                <MenuItem eventKey={3.1} onClick={this.props.onLogout}>
                    <NavItem className='nav-item' onClick={this.props.onLogout}>LOGOUT</NavItem>
                </MenuItem>
            </Nav>
        } else
            rightMenu = <Nav pullRight className='navbar_header'>
                <MenuItem eventKey={3.1}>
                    <Link to={'/login'}>SIGN IN</Link>
                </MenuItem>
                <MenuItem eventKey={3.2}>
                    <Link to={'/signup'}>SIGN UP</Link>
                </MenuItem>
            </Nav>;

        return (
            <Navbar>

                {rightMenu}

            </Navbar>
        )
    }

}



export default Menu;
