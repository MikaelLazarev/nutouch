import React from 'react';
import { Route, Switch, Redirect } from 'react-router';
import Menu from '../Menu/Menu'
import Footer from './Footer'
import Login from "../../containers/Auth/Login";
import SignUp from "../../containers/Auth/SignUp";
import Page404 from "../HelpPage/404";
import DevicesList from "../../containers/Devices/DevicesList";
import DevicesRegister from '../../containers/Devices/DevicesRegister'

const layout = (props) => {
    /** layout manages routing in frontend app */

    let routes;

    if (!props.isAuthenticated) {
        routes = <Switch>
            <Route path="/login/" component={Login}/>
            <Route path="/signup/" component={SignUp}/>
            <Route component={Login}/>
        </Switch>
    }
    else {
        routes = <Switch {...props}>

                    {/* Investors menu */}
                    <Route exact path="/devices/" component={() => <DevicesList {...props} />}/>
                    <Route exact path="/register/" component={() => <DevicesRegister {...props} />}/>

                    {/* Default action & Page 404 */}
                    <Route exact path="/" render={() => (<Redirect to="/devices"/>)}/>
                    <Route exact path="/login" render={() => (<Redirect to="/devices"/>)}/>
                    <Route path='*' component={Page404}/>
        </Switch>
        }

    return (<React.Fragment>
                <Menu {...props} />
                    {routes}
                <Footer {...props}/>
            </React.Fragment>);
}

export default layout;
