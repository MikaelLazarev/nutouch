import React from 'react'

const DeviceListItem = (props) => {

    const option = props.data;
    const href = '/chats/' + option.label

    // Search is moved to component level cause we have data for projects and for options
    // And have to search in this fields
    if (props.search !== undefined) {
        const search = props.search
        // If search criteria is set up return nothing if we could not it found
        if ((option.name.toLowerCase().indexOf(search) === -1)) {
            return <React.Fragment/>
        }
    }

    console.log('OOO', option)


    return(
        <tr key={ option.optionTokenContractAddress } >

            <td align="left">
                    <strong onClick={() => props.choose_chat_func(option.label)}>
                         { option.name }
                        </strong><br />
            </td>
        </tr>
        );

}

export default DeviceListItem;
