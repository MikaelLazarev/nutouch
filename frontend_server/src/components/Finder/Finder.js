import React from "react";
import {
  FormGroup,
  Form,
  FormControl,
  Col,
  Row,
  Panel,
  Table,
}
from 'react-bootstrap'
import './Finder.css'

import DeviceListItemHeader from './DeviceListItemHeader'
import DeviceListItem from "./DeviceListItem"


export class Finder extends React.Component {

  state = {
    table_data: '',
    search: '',
  }

  searchHandler(event) {
    event.preventDefault();
    const value = event.target.value.toLowerCase();
    this.setState({ search: value });
  }

  render() {

    const items = Object.values(this.props.itemsList.data)
    let headerItem, finderItem;
    let type = (this.props.type === undefined) ? 'MarketplaceOptionItem' : this.props.type;

        headerItem = <DeviceListItemHeader/>

    return (

      <Panel>
                    <Panel.Body>
                    <Row style={{marginBottom: '15px'}}>
                      <Col sm={8}>
                        <h2 className="Panel-title" >
                          {this.props.header}
                        </h2>
                        </Col>
                        <Col sm={4} style={{textAlign: 'right'}}>
                          <Form inline >
                            <FormGroup align="right" >
                              <FormControl
                                type="text"
                                id='search'
                                placeholder="Search"
                                style={{backgroundColor: '#FFFFFF', width: '100%', marginTop: '8px'}}
                                onKeyUp={(e)=>this.searchHandler(e)}
                                >

                             </FormControl>
                            </FormGroup>

                            </Form>
                        </Col>
                        </Row>
                      <Table responsive striped hover>
                        <thead>
                            { headerItem }
                        </thead>
                        <tbody>
                          { items.map(dataRow => {

                              switch (type) {

                                  case 'DeviceListItem':
                                  default:
                                      finderItem = <DeviceListItem
                                          data={dataRow}
                                          key={dataRow.label}
                                          search={this.state.search}
                                          choose_chat_func = { this.props.choose_chat_func }/>
                                      break;
                              }
                              return finderItem
                          })}


                        </tbody>

                        </Table>
                     </Panel.Body>
                    </Panel>

    );
  }
}


export default Finder;
