# NuTouch - Explore new services without sharing personal info

SCREENCAST HOW IT WORKS: https://youtu.be/gS3jzbZCd34

This project was especially created for Nucypher Serial Hachathon (https://serialhackaton.com/) from scratch.

## Problem
With enomourlsy quantity of spam and annoying marketing activities, users usually afraid to provide their emails (or other personal info) during registration to unknown services. As result, it creates a huge gap, make difficulties to new services to come to market and makes conversation from interested to registered users very low.

## Solution
Nutouch improves signing up and logging experinces. The idea is to provide access to service immidiately (in one click) and ask for email & needed credentails further when the trust will be established. Users sign up and login with one click ("Touch with Nutouch") without providing their credetials or personal information. 

When user is registered with Nutouch option to one site, all other user devices are automatically get an access to this service with the same account. User could manage access options using Access Manager Service.

## What's in package

extension/ - chrome browser extention to start using NuTouch

backend_server, frontend_server/ - NuTouch Access Server, which helps users to manage access to different devices

backend_client, frontend_client/ - Demo server which use Nutouch technology which shows automatically generated username

## User experience

1. Download and install chrome extention on all your devices. The extension automatically generates secret keys and store them.
2. Sign up to Nutouch Access Manager Service and connect all your devices into account
3. Use Touch Nutouch button on services which support this technology (Test service could be found as backend_client / frontend_client) to sign up and login in one click!

## How it works

![](achitecture.jpg)

1. When chrome user opens a new page, *background.js* sends a request with *TabId* to *content.js* (content layer)
2. *Content.js* stores it's own *TabId*
3. When *page.js* is loaded, it sends a message to *content.js* that page is ready to nutouch
4. *Content.js* send URL to *background.js*, which resend it to *Native Messaging app*
5. *Native messaging app* receives *URL* and sends it to Nutouch *Access Manager Server* with Reader pubkey (pubkey of specific extension). Access Manager Server check requested *URL* and returns *ChannelSecret* for this *URL* for this user (All user devices could read and decrypt messages from this *Channel*. If there is no *URL* was found in *Access Manager Server Database*, it creates a *New Channel*.
6. *Native messaging app* receives *ChannelSecret* and sends a request with *ChannelSecret* to *Client Server* (which is known from *URL*)
7. *Client Server* receives *ChannelSecret* and checks a user with *ChannelSecret* username. If no, it creates it. 
8. For this user *Client server* generates *JWT Tokens*, encrypt them with *ChannelSecret* and sends them back to *Native Messaging App*
9. *Native Messaging App* receives *Encrypted JWT tokens* and decrypt them
10. *Native messaging App* sends *decrypted JWT tokens* back to *background.js*
11. *Backgound.js* receives *JWT tokens* and sends them to *content.js*.
12. *Content.js* sends *JWT tokens* to *page.js*
13. *Page.js* injects *JWT tokens* into *window.nutouch* variable
14. *Client Frontend* accesses *JWT tokens* from *window.nutouch* and use them for authenfication.




